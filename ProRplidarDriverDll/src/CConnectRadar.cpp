#include "CConnectRadar.h"
#include "../sdk/src/hal/types.h"

#pragma warning(disable : 4996)

CConnectRadar::CConnectRadar():
	_isConnected(false)
{
}

CConnectRadar::~CConnectRadar()
{
	funOnDisconnect();
}

CConnectRadar& CConnectRadar::funGetInstance()
{
	static CConnectRadar instance;
	return instance;
}

bool CConnectRadar::funOnConnect(std::string port, int baudrate)
{
	if (_isConnected)return true;

	_channel = (*createSerialPortChannel(port, baudrate));

	if (!_lidarDrv)
	{
		_lidarDrv = *createLidarDriver();

		if (!_lidarDrv)
		{
			return SL_RESULT_OPERATION_FAIL;
		}
	}

	if (SL_IS_FAIL(_lidarDrv->connect(_channel)) || SL_IS_FAIL(_lidarDrv->getDeviceInfo(_devinfo)))
	{
		return _isConnected;
	}

	_isConnected = true;
	if (!funCheckDeviceHealth())
	{
		_isConnected = false;
	}

	return _isConnected;
}

void CConnectRadar::funOnDisconnect()
{
	if (_isConnected)
	{
		_lidarDrv->stop();
	}

	_isConnected = false;

	delete _lidarDrv;
	_lidarDrv = nullptr;
}

void CConnectRadar::funOnCreateData()
{
	_modeVector.clear();
	_lidarDrv->getAllSupportedScanModes(_modeVector); // 获取支持的扫描模式
	_lidarDrv->getTypicalScanMode(_usingScanMode);    // 获取默认使用的模式

	MotorCtrlSupport motorSupport;
	_lidarDrv->checkMotorCtrlSupport(motorSupport);

	_motorInfo.motorCtrlSupport = motorSupport;
	_lidarDrv->getMotorInfo(_motorInfo);
}

void CConnectRadar::funRunRadar()
{
	_lidarDrv->setMotorSpeed();
	_lidarDrv->startScanExpress(false, _usingScanMode); // 开始扫描
}

void CConnectRadar::funPauseRadar()
{
	_lidarDrv->stop();
}

void CConnectRadar::funStopRadar()
{
	_lidarDrv->stop();
}

void CConnectRadar::funResetRadar()
{
	_lidarDrv->reset();
}

void CConnectRadar::funSetMotorSpeed(int precent)
{
	_lidarDrv->setMotorSpeed(precent);
}

int CConnectRadar::funGetUsingScanMode()
{
	return _usingScanMode;
}

int CConnectRadar::funGetMotorInfo(int id)
{
	if (id == 0)
	{
		return _motorInfo.max_speed;
	}
	else
	{
		return _motorInfo.desired_speed;
	}
}

std::string CConnectRadar::funGetModeStr(int id)
{
	return _modeVector[id].scan_mode;
}

float CConnectRadar::funGetSampleDuration()
{
	return _sampleDuration;
}

float CConnectRadar::funGetScanSpeed()
{
	return _scanSpeed;
}

ILidarDriver* CConnectRadar::funGetILidarDriver()
{
	return _lidarDrv;
}

void CConnectRadar::funGetScanData(std::vector<scanDataArr>& data)
{
	sl_lidar_response_measurement_node_hq_t nodes[8192];
	size_t cnt = _countof(nodes);

	if (IS_OK(_lidarDrv->grabScanDataHq(nodes, cnt, 0)))
	{
		float sampleDurationRefresh = _modeVector[_usingScanMode].us_per_sample;
		
		data.clear();
		_isScanning = true;
		for (int pos = 0; pos < (int)cnt; ++pos) {
			scanDataArr dot;
			if (!nodes[pos].dist_mm_q2) continue;

			dot.quality = nodes[pos].quality;
			dot.angle = nodes[pos].angle_z_q14 * 90.f / 16384.f;
			dot.dist = nodes[pos].dist_mm_q2 / 4.0f;
			data.push_back(dot);
		}

		_sampleDuration = sampleDurationRefresh;
		_scanSpeed = 1000000.0f / (cnt * sampleDurationRefresh);
	}
}

std::string CConnectRadar::funGetSerialNumber()
{
	std::string serialNumber;
	char titleMsg[200];
	for (int pos = 0, startpos = 0; pos < sizeof(_devinfo.serialnum); ++pos)
	{
		sprintf(&titleMsg[startpos], "%02X", _devinfo.serialnum[pos]);
		startpos += 2;
	}
	serialNumber = titleMsg;

	return serialNumber;
}

bool CConnectRadar::funIsConnected()
{
	return _isConnected;
}

bool CConnectRadar::funCheckDeviceHealth(int* errorcode)
{
	int errcode = 0;
	bool ans = false;

	do {
		if (!_isConnected) {
			errcode = SL_RESULT_OPERATION_FAIL;
			break;
		}

		sl_lidar_response_device_health_t healthinfo;
		if (IS_FAIL(_lidarDrv->getHealth(healthinfo))) {
			errcode = SL_RESULT_OPERATION_FAIL;
			break;
		}

		if (healthinfo.status != SL_LIDAR_STATUS_OK) {
			errcode = healthinfo.error_code;
			break;
		}

		ans = true;
	} while (0);

	/* 如果出现问题 */
	if (!ans)
	{
		// do
		*errorcode = errcode;
	}

	return ans;
}
